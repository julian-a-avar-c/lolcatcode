// @GENERATOR:play-routes-compiler
// @SOURCE:conf/routes

import play.api.mvc.Call

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:7
package controllers {

    // @LINE:10
    class ReverseAssets(_prefix: => String):
        def _defaultPrefix: String =
            if _prefix.endsWith("/") then "" else "/"

        // @LINE:10
        def versioned(file: Asset): Call =
            implicit lazy val _rrc =
                new play.core.routing.ReverseRouteContext(Map((
                    "path",
                    "/public"
                ))); _rrc
            end _rrc
            Call(
                "GET",
                _prefix + { _defaultPrefix } + "assets/" + implicitly[
                    play.api.mvc.PathBindable[Asset]
                ].unbind("file", file)
            )
        end versioned
    end ReverseAssets

    // @LINE:7
    class ReverseHomeController(_prefix: => String):
        def _defaultPrefix: String =
            if _prefix.endsWith("/") then "" else "/"

        // @LINE:7
        def index(): Call =
            Call("GET", _prefix)
    end ReverseHomeController

}
