package views.html

import play.api.i18n.*
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Xml
import models.*
import play.api.mvc.*
import controllers.*
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.TwirlFeatureImports.*
import _root_.play.twirl.api.Txt
import play.api.templates.PlayMagic.*
import views.html.*
import _root_.play.twirl.api.TwirlHelperImports.*
import play.api.data.*

object index
    extends _root_.play.twirl.api.BaseScalaTemplate[
        play.twirl.api.HtmlFormat.Appendable,
        _root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]
    ](play.twirl.api.HtmlFormat)
    with _root_.play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable]:

    /**/
    def apply /*1.2*/ (): play.twirl.api.HtmlFormat.Appendable =
        _display_ {
            {

                Seq[Any](
                    format.raw /*2.1*/ ("""
"""),
                    _display_( /*3.2*/ main("Welcome to Play") /*3.25*/ {
                            _display_(Seq[Any](
                                format.raw /*3.27*/ ("""
  """),
                                format.raw /*4.3*/ ("""<h1>Welcome to Play!</h1>
""")
                            ))
                        }),
                    format.raw /*5.2*/ ("""
""")
                )
            }
        }

    def render(): play.twirl.api.HtmlFormat.Appendable = apply()

    def f: (() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

    def ref: this.type = this
end index

/*
                  -- GENERATED --
                  SOURCE: app/views/index.scala.html
                  HASH: a4a66cce550b5a1152c9366b663a0ef572a4531f
                  MATRIX: 722->1|818->4|845->6|876->29|915->31|944->34|1000->61
                  LINES: 21->1|26->2|27->3|27->3|27->3|28->4|29->5
                  -- GENERATED --
 */
