import mill._, scalalib._, publish._
import $ivy.`com.lihaoyi::mill-contrib-playlib:`,  mill.playlib._

object lolcatcode extends RootModule with PlayModule with PublishModule {
    def publishVersion = "0.0.0"

    def pomSettings = PomSettings(
        description = "Free 1337z for everyone in every language",
        organization = "com.julian-a-avar-c",
        url = "https://github.com/julian-a-avar-c/lolcatcode",
        licenses = Seq(License.`MPL-2.0`),
        versionControl = VersionControl.github("julian-a-avar-c", "lolcatcode"),
        developers = Seq(
            Developer("julian-a-avar-c", "Julián A. Avar C.", "https://github.com/julian-a-avar-c")
        )
    )

    def scalaVersion = "3.3.1"
    def playVersion = "3.0.1"
    def twirlVersion = "2.0.1"

    object test extends PlayTests
}
